#!/bin/bash
declare -i steprule
resultfile=ThresholdEffectIndexumber_
for j in {1..5..1}
do
for i in {0..6..1}
do
for rule in  {100..10000..50}
do
	update=10
	Excution=-1
	declare -i support
	if [ "$i" -eq 0 ]
		then 
	support=$(($rule/45))
	fi
	if [ "$i" -eq 1 ]
		then 
	support=$(($rule/40))
	fi
	if [ "$i" -eq 2 ]
		then 
	support=$(($rule/35))
	fi
	if [ "$i" -eq 3 ]
		then 
	support=$(($rule/30))
	fi
	if [ "$i" -eq 4 ]
		then 
	support=$(($rule/25))
	fi
	if [ "$i" -eq 5 ]
		then 
	support=$(($rule/20))
	fi
	if [ "$i" -eq 6 ]
		then 
	support=$(($rule/15))
	fi
	test=$support
	while [ "$Excution" -ne 0 ]
		do
		echo "rule : $rule ; threshold : $test ; update : $update; threshold : $support "
		java -jar ofcomfpiler2.jar ./result/$resultfile$i ./input/benchmark_$rule.rcf ./input/benchmark_up_$update.rcf $test 1 $rule $update
		Excution=$?		
		test=$(($test+1))
		if [ $test -ge $(($support+10)) ]
			then
			break
		fi
		echo "Excution result is $Excution"
	done
done
done
done
rm -rf *~
