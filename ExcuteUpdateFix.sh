#!/bin/bash
declare -i steprule
resultfile=FormatResult
for j in {1..2..1}
do
for update in  {150..500..150}
do
for rule in {100..10000..100}
do
	support=$(($rule/35))
	test=$support
	Excution=-1
	while [ "$Excution" -ne 0 ]
		do
		echo "rule : $rule ; threshold : $test ; update : $update; threshold : $support "
		java -jar ofcomfpiler2.jar ./result/$resultfile$update ./input/benchmark_$rule.rcf ./input/benchmark_up_$update.rcf $test $(($update/10)) $rule $update
		Excution=$?		
		test=$(($test+1))
		if [ $test -ge $(($support+10)) ]
			then
			break
		fi
		echo "Excution result is $Excution"
	done
done
done
done
rm -rf *~
