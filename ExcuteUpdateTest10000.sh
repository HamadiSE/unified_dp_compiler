#!/bin/bash
declare -i steprule
resultfile=Rules_10000_up_per10_MultiTen
rule=10000
declare -i update
support=360
for i in {1..10..2}
do
for update in `eval echo {10..2500..10}`
	do
	suppUpdate=$((update/10))
	supp=$suppUpdate
	Excution=-1
		while [ "$Excution" -ne 0 ]
			do
			echo "Rule $rule ; Update $update"
			java -jar ofcomfpiler1.jar ./result/$resultfile ./input/benchmark_$rule.rcf ./input/benchmark_up_$update.rcf $support $supp $rule $update 
			Excution=$?		
			supp=$(($supp+1))
			if [ $supp -ge $(($suppUpdate+10)) ]
				then 
				break
			fi
		done
done
done
rm -rf *~

