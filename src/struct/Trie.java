package struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


class Node {
	boolean leaf;
	Lat_Attribute key;
	ArrayList<Node> childs;
	Node parent;
	int level;
	ArrayList<Lat_Object> ruleSubset;
	public Node(boolean bool,Lat_Attribute key,ArrayList<Node> childs, Node parent, int level) {
		this.leaf = bool;
		this.key = key;
		this.childs = childs;
		this.parent = parent;
		this.level=level;
		this.ruleSubset = new ArrayList<Lat_Object>();
	}
	public Node() {
		this.leaf = false;
		this.key = new Lat_Attribute(0);
		this.parent = null;
		this.childs = new ArrayList<Node>();
		this.level=0;
		this.ruleSubset = new ArrayList<Lat_Object>();
	}
	@Override
	public String toString() {
		String string = "";
			if(this.level==0){
				string = "*\n";
			}else{
				string =this.key.toString()+" "+this.ruleSubset+"\n";
				}
			//if(this.childs.size()==1) return string;
			for(int i=0;i<this.childs.size();i++){
				String l="";
				for(int j=0;j<this.childs.get(i).level-1;j++){
					l+="\t";
				} l+="|------*";
			string +=l+this.childs.get(i).toString()+"\n";
			}			
		return string;
	}	
}

public class Trie {
	public Node root;

	public Trie() {
		this.root = new Node();		
	}
	public void CompressTrie(Node node){
		if(node.level!=0 && node.childs.size()==1){
			//node.parent.childs.remove(node);
			//node.childs.get(0).parent=node.parent;
			node.childs.get(0).level--;
			node.childs.get(0).key=node.key;
			//node.parent.childs.add(node.childs.get(0));
			node.parent.childs.remove(node);

			node.childs.get(0).parent = node.parent;
			node.parent.childs.add(node.childs.get(0));
			for(int i=0;i<node.parent.childs.size();i++){
				CompressTrie(node.parent.childs.get(i));
			}
		}else	{	
		for(int i=0;i<node.childs.size();i++){
			CompressTrie(node.childs.get(i));
		}
		}
	}
	
	
	public Lat_Attribute FindMaxAttr(ArrayList<Lat_Attribute> list_attr){
		int res =0;
		int max=0;
		
		for(int j=0;j<list_attr.size();j++){
			if(list_attr.get(j).weight>max){
				max=list_attr.get(j).weight;
				res=j;
			}
		}
		return list_attr.get(res);
	}	
	public Index FindMaxIndex(ArrayList<Index> indexList){
		int res=0;
		int max=0;
		for(int j=0;j<indexList.size();j++){
			if(FindMaxAttr(indexList.get(j).key).weight>max){
				max=FindMaxAttr(indexList.get(j).key).weight;
				res=j;		
			}
		}
		return indexList.get(res);
	}
	public Node LookupIndex(Node node, Index index){
		if(node.level==0){
			//This means we are in the root
			//System.out.println("root "+node.key);
			for(int e=0;e<node.childs.size();e++){
				//System.out.println("root node key "+node.childs.get(e).key);
				if(index.key.contains(node.childs.get(e).key)){
					//System.out.println("Something found in root "+node.childs.get(e).key+" level "+node.childs.get(e).level);
					node=node.childs.get(e);
					LookupIndex(node,index);
				}
			}
			//System.out.println("return root");
			return node;
		}				
		if(index.key.contains(node.key)){
			//System.out.println("\t The search is here "+node.key+" level "+node.level);
			if(node.leaf!=true){
				//System.out.println("Something found till now "+node.key);
				for(int e=0;e<node.childs.size();e++){
					//System.out.println("e             "+node.childs.get(e).key);
					if(index.key.contains(node.childs.get(e).key)){						
						LookupIndex(node.childs.get(e),index);
					}
				}
				//System.out.println("return node "+node.key);
				return node;
			}			
		}		
			return node;
	}
	 public void editIndexList(ArrayList<Index> indexList){
		 Index idx = indexList.get(2);
		 indexList.add(idx);
	      /*  for (Index i: indexList) {
	            if (i.id == 1) {
	                i.key.get(0).type = indexList.get(2).key.get(0).type;
	                i.key.get(0).value = indexList.get(2).key.get(0).value;
	            }
	        }*/
	    }
	public void CreateTrie(ArrayList<Index> indexList){
		//editIndexList(indexList);
		// fix weight problem and use it as base
		for(int i=0;i<indexList.size();i++){
			for(int j=i;j<indexList.size();j++){
				Index index1 = indexList.get(i);
				Index index2 = indexList.get(j);
				for(int e=0;e<index1.key.size();e++){					
					for(int f=0;f<index2.key.size();f++){
						if(index1.key.get(e).equals(index2.key.get(f))){
							index1.key.get(e).weight++;
							//index2.key.get(f).weight++;
						}
							
					}
				}
			}
		}
		
		
		
		HashMap<Integer, ArrayList<Index>> index_level = new HashMap<Integer, ArrayList<Index>>();
		java.util.Iterator<Index> iterindex = indexList.iterator();
		while(iterindex.hasNext()){
			Index index = iterindex.next();
			int num_fields = index.key.size();
			//System.out.println(num_fields);
			if(index_level.containsKey(num_fields)){
				index_level.get(num_fields).add(index);
			}
			else{
				ArrayList<Index> temp = new ArrayList<Index>();
				temp.add(index);
				index_level.put(num_fields, temp);
			}			
			if(num_fields==1){
				ArrayList<Node> tempNodeList = new ArrayList<Node>();
				Node node_tmp = new Node(true, index.key.get(0), tempNodeList, this.root,1);
				node_tmp.ruleSubset.addAll(index.ruleSubset);
				this.root.childs.add(node_tmp);
			}
		}
		for(int i=0;i<index_level.size()-1;i++){
		//for(int i=0;i<1;i++){
			//in i=0; we will retrieve the level 2.
			// the first level is already added so we need to start from level 2.
			ArrayList<Index> current_level = index_level.get(i+2);
			int til = current_level.size();
			for(int j=0;j<til;j++){
				Index index = FindMaxIndex(current_level);
				Node node = new Node();
				//node.parent = new Node();
				//System.out.println("\t\tindex to search "+index);
				node = LookupIndex(this.root,index);
				//System.out.println("\t\tEnd of search\n\n");
				//node = this.root.childs.get(0);
				//System.out.println("The result of the search is "+node.key);
				//System.out.println("THE SEARCH RESULT WAS "+node+"END OF NODE\n");
				   //System.out.println("And here "+this.root.childs.size());
				
						//Node nodet = LookupIndex(this.root,index);
						Node nodet = new Node();
						nodet = node;
						ArrayList<Lat_Attribute> node_index_list = new ArrayList<Lat_Attribute>();						
						//node_index_list.add(nodet.key);
						while(nodet.parent!=null){
							node_index_list.add(node.key);
							nodet = nodet.parent;
						}
						ArrayList<Lat_Attribute> remainAtt = new ArrayList<Lat_Attribute>();
						for(int k=0;k<index.key.size();k++){
							if(!node_index_list.contains(index.key.get(k))){
								remainAtt.add(index.key.get(k));
							}
						}
			//if(remainAtt.size()>2) continue;
			int cst = remainAtt.size();
			for(int k=0;k<cst;k++)	{
				//System.out.println("remain"+remainAtt);				
				Lat_Attribute lat_attr = FindMaxAttr(remainAtt);
				//if(k>0 && lat_attr.weight==1) break;
				ArrayList<Node> tempNodeList = new ArrayList<Node>();
				if(remainAtt.size()>1) {
					
					Node temp = new Node(true, lat_attr,tempNodeList,node,node.level+1);
					node.childs.add(temp);
					node.leaf=false;
					node=temp;					
				}
				if(remainAtt.size()==1){
					
					Node temp = new Node(true, lat_attr,tempNodeList,node,node.level+1);
					temp.ruleSubset.addAll(index.ruleSubset);
					node.childs.add(temp);
					node.leaf=false;
					node=temp;
				}
				
				remainAtt.remove(lat_attr);
			} current_level.remove(index);
			}
		}
	}	
}
