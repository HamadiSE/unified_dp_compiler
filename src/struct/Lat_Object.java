package struct;

public class Lat_Object {
	
	public int id;
	public String name;
	
	public Lat_Object(int id, String name){
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return name ;
	}
}
