package struct;

public enum AttributeType {
	
	IngressPort,
	IngressVPort,
	EtherSrc,
	EtherDst,
	EtherType,
	VlanId,
	IpSrc,
	IpDst,
	PortSrc,
	PortDst;
}
