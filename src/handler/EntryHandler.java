package handler;

import java.lang.Character.Subset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import java.util.Iterator;

import struct.Application;
import struct.Entry;
import struct.AttributeType;
import struct.Index;
import struct.Lat_Attribute;
import struct.Lat_Object;
import struct.Lattice;
import struct.Pipeline;
import struct.Rule;
import struct.SubsetTable;
import struct.Table;

public class EntryHandler {

	public ArrayList<SubsetTable> generateSubsetTables( Lattice lattice, ArrayList<Index> indexList, Map<Integer, Rule> rulesList ){
		ArrayList<SubsetTable> subsetTableList = new ArrayList<SubsetTable>();
		ArrayList<Lat_Attribute> allAttributsCache = new ArrayList<Lat_Attribute>();
		for (Map.Entry<Integer,Lat_Attribute> e : lattice.attributes.entrySet()){
			allAttributsCache.add(e.getValue());
		}
		for (int i=0; i<indexList.size(); i++){
			// Recuperer la liste des attributs de l'index 
			Set<Lat_Attribute> attrIndexCache = new HashSet<Lat_Attribute>();
			for (int j=0; j<indexList.get(i).key.size(); j++){
				attrIndexCache.add(indexList.get(i).key.get(j));
			}
			SubsetTable subsetTable = new SubsetTable (i+1000);
			// Recuperer les attributs pour completer la regle
			ArrayList<AttributeType> fieldTypeStack = new ArrayList<AttributeType>();
			ArrayList<Lat_Object> ruleSubset = indexList.get(i).ruleSubset;
			for (int j=0; j<ruleSubset.size(); j++){
//				////System.out.println("rule subset "+ruleSubset.get(j));
//				////System.out.println(rulesList.get(1));
				Rule rule = rulesList.get( ruleSubset.get(j).id-1 );
//				////System.out.println("first "+ruleSubset.get(j).id);
//				////System.out.println(rule.id);
				for (int k=0; k<allAttributsCache.size(); k++){
					Lat_Attribute attribut = allAttributsCache.get(k);
					if ( !fieldTypeStack.contains(attribut.type) ){
						fieldTypeStack.add(attribut.type);
						if (attrIndexCache.contains(attribut)){
							rule.matchfields.remove(attribut.id);
						} else {
							Lat_Attribute newAttribut = new Lat_Attribute(attribut.id);
							newAttribut.type = attribut.type;
							newAttribut.value = "*";
							//////System.out.println(rule.id);
							rule.matchfields.put(newAttribut.id, newAttribut);
						}
					}
				}
				subsetTable.rules.add(rule);
			}
			subsetTableList.add(subsetTable);
		//	////System.out.println("adding table with id " +subsetTable.id);
		}
		return subsetTableList;
	}
	public ArrayList<Pipeline> generateIndexPipeline(ArrayList<Index> indexList){
		ArrayList<Pipeline> pipelineList = new ArrayList<Pipeline>();
		Map<AttributeType, Table> tableList = new HashMap<AttributeType, Table>();
		int idTable = 0;
		for(int i=0;i<indexList.size();i++){
			Index index = indexList.get(i);
			Pipeline pipeline = new Pipeline(index.id);
			//////System.out.println("index is "+index); //works fine
			for(int j=0;j<index.key.size();j++){
				Lat_Attribute matchfield = index.key.get(j);
				Table table;
				if(tableList.containsKey(matchfield.type)){
					table = tableList.get(matchfield.type);
				}
				else{
					table = new Table(idTable, matchfield.type);
					idTable++;
				}
				// Create Entry
				String action;
				int tempId ;
				int in_metadata = 'N';
				int out_metadata = 'X';
				if(j+1<index.key.size()){					
					action ="GO_TO_TABLE";
					if(tableList.containsKey(index.key.get(j+1))){
						tempId	= tableList.get(index.key.get(j+1)).id; 						
					}
					else{
						tempId = idTable; 						
					}
					out_metadata = index.id;
				}
				else {
					tempId = index.id+1000; 
					action = "GO_TO_SUBTABLE";
				} 
				  
				  if(j!=0) in_metadata = index.id;
				  
					Entry entry = new Entry(index.id, matchfield, action, tempId, in_metadata,out_metadata);
				//////System.out.println("entry created is "+entry);
				//if(table.id==0) ////System.out.println("Table "+table.id+" add entry "+entry.id);
				table.entry.add(entry);
				if(!tableList.containsKey(matchfield.type)){
					tableList.put(matchfield.type, table);
				}
				//if(test) {
					if(pipeline.tables.containsKey(table.id)) pipeline.tables.remove(table.id);
					pipeline.tables.put(table.id, table);
					//////System.out.println("Pipeline "+pipeline.id+ " A table has been added "+table.id);
				//}
			} pipelineList.add(pipeline);
		}
		
		return pipelineList;
	}
	public int[][] generateMatrix(ArrayList<Pipeline> pipelineList){
		int[][] matrixTabTab = new int[32][32];
		int[][] matrixTabSub = new int[32][10000];
		Set<Integer> setTab = new HashSet<Integer>() ;
		Set<Integer> setSub = new HashSet<Integer>() ;
		Iterator<Pipeline> pipeIter = pipelineList.iterator();
		while(pipeIter.hasNext()){
			Pipeline pipeline = pipeIter.next();
			for(Map.Entry<Integer, Table> Element : pipeline.tables.entrySet()){
				Table table = Element.getValue();
				Iterator<Entry> entryIter = table.entry.iterator(); 
				while(entryIter.hasNext()){
					Entry entry = entryIter.next();
					String action = entry.action;
					if(action == "GO_TO_TABLE"){
						matrixTabTab[table.id][entry.nextTableId]=1;
						setTab.add(table.id);
						setTab.add(entry.nextTableId);
					}
					if(action == "GO_TO_SUBTABLE"){
						matrixTabSub[table.id][entry.nextTableId-1000]=1;
						setTab.add(table.id);
						setSub.add(entry.nextTableId);
					}
				}
			}
		};
		int[][] finalMatrix = new int[setTab.size()][setTab.size()+setSub.size()];
		for(int i=0;i<setTab.size();i++){
			for(int j=0;j<setSub.size()+setTab.size();j++){
				if(j<setTab.size()){
					finalMatrix[i][j]=matrixTabTab[i][j];
				}
				else{
					finalMatrix[i][j]=matrixTabSub[i][j-setTab.size()];
				}
			}
		}
		return finalMatrix;		
	}
	
	public Application appGenerator( Lattice lattice, ArrayList<Index> indexList, Map<Integer, Rule> rulesList ){
		ArrayList<SubsetTable> SubTab = generateSubsetTables(lattice,indexList,rulesList);
		ArrayList<Pipeline> pipelineList = generateIndexPipeline(indexList);
		
		
		Map<Integer,SubsetTable> MapSubTab = new HashMap<Integer,SubsetTable>();
		Map<Integer,Table> MapTable = new HashMap<Integer,Table>();
		
		int e=0;
		Iterator<SubsetTable> subtTabIter = SubTab.iterator();
		while(subtTabIter.hasNext() && e<(SubTab.size()+1)){
			SubsetTable table = subtTabIter.next();
			MapSubTab.put(e, table);
			e++;
			//////System.out.println("table id is "+table.id);
		}
		ArrayList<Pipeline> PipeList = generateIndexPipeline(indexList);
//		////System.out.println("Pipeline List generated");
		e=0;
		Iterator<Pipeline> pipeIter = PipeList.iterator();
		while(pipeIter.hasNext() && e<100){
			e++;
			for(Map.Entry<Integer, Table> table : pipeIter.next().tables.entrySet()){
				MapTable.put(table.getValue().id, table.getValue());
				//////System.out.println("tab id is "+table.getValue().id);
			}
		}
//		////System.out.println("End of iterrating over Pipeline");
		int[][] tableChaining = generateMatrix(pipelineList);
//		////System.out.println("Matrix generated");
		
		Application app = new Application(0, MapTable, MapSubTab, tableChaining);
		return app;		
	}

	
	
	
	
	
	
	
	
	
	
/*	public ArrayList<Pipeline> generateIndexPipelines(ArrayList<Index> indexList){
		ArrayList<Pipeline> pipelineList = new ArrayList<Pipeline>();
		Map<AttributeType, Table> tableList = new HashMap<AttributeType, Table>();
		int idTable = 0;
		for (int i=0; i<indexList.size(); i++){ //Each index has its own table
			Index index = indexList.get(i);
			//if(index == null) continue;
			Pipeline piepline = new Pipeline(index.id);
			int lastPieplineTableId = 0;
			for (int j=0; j<index.key.size(); j++){ //Each key should be added to the relevant table if it exist, or create a new one
				// Get the table that uses the same fieldType
				AttributeType fieldType = index.key.get(j).type;
				Table table;
				if (tableList.containsKey(fieldType)){
					table = tableList.get(fieldType);
				} else {
					table = new Table(idTable, fieldType);
					table.setTableMatchType(fieldType);
				}
				// Create entry
				int idEntry = index.id;
				Lat_Attribute matchfield = index.key.get(j);
				int nextTableId;
				String action;
				if (index.key.size() - j > 1)
				{ //Not the last element of keys in the index
					Lat_Attribute nextMatchfield = index.key.get(j+1);
					if (tableList.containsKey(nextMatchfield.type)){
						nextTableId = tableList.get(nextMatchfield.type).id;
					} else {
						nextTableId = lastPieplineTableId + 1;
					}
					action = "GOTO_TABLE_"+ nextTableId;
				} else {
					//The last element of keys in the index
					nextTableId = index.id;
					action = "GOTO_RULE_SUBSET_TABLE_" + nextTableId;
				}
				Entry entry = new Entry(idEntry, matchfield, action, nextTableId,0,0);
				table.entry.add(entry);
				if (!tableList.containsKey(fieldType)){
					tableList.put(fieldType, table);
					idTable ++;
				}
				lastPieplineTableId = idTable;
				// Insert table in pipeline
				piepline.tables.put(table.id, table);
			}
			////System.out.println(piepline);
			pipelineList.add(piepline);
		}
		return pipelineList;
	}
	*/
}
