package handler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import struct.*;



public class Compiler {
	public static void main(String[] args) throws InterruptedException, IOException  {
		//Input Params for the program
		
		String resultFilename = args[0];
		String inputFilenameRCF = args[1];
		String inputFilenameRCF_up = args[2];
		int threshold = Integer.parseInt(args[3]);
		int updateThreshold = Integer.parseInt(args[4]);
		int numberOfRules = Integer.parseInt(args[5]);
		int numberOfUpdates = Integer.parseInt(args[6]);		
		int objIdOff = 0;
		String ofcompilerpath = "/var/www/demo/compiler/";
		
		
	
		
		/*
				
		String resultFilename = "fileResult";//args[0];
		String inputFilenameRCF = "input/benchmark_up_10.rcf"; //args[1];
		String inputFilenameRCF_up = "input/benchmark_up_10.rcf";//args[2];//"input/test_up10.rcf";
		int threshold =2; //Integer.parseInt(args[3]);//
		int updateThreshold = 2;//Integer.parseInt(args[4]);
		int numberOfRules = 21; //Integer.parseInt(args[5]);
		int numberOfUpdates = 21;//Integer.parseInt(args[6]);		
		int objIdOff = 0;
		String ofcompilerpath = "/root/workspace/OFcompiler/";
		*/
		
		
		//print input file
		BufferedReader br = new BufferedReader(new FileReader(inputFilenameRCF)) ;
			PrintWriter writerrcf = null;			
			//writerrcf = new PrintWriter(new FileWriter("fileInput/file.rcf",false));
			writerrcf = new PrintWriter(new FileWriter(resultFilename,false));
			   String line = null;
			//  while ((line = br.readLine()) != null) {
			//	   writerrcf.println(line);
			//   }
			   br.close();
			   //writerrcf.close();
			
		//Store result
		String result="";
		String resultform ="";
		result = numberOfRules+"\t"+threshold+"\t"+numberOfUpdates+"\t";
		resultform = "Rules number\t threshold \t Updates number\t";
		//Create file to store Statistic
		float timeBase =1000000; 
		File f = new File(resultFilename);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new FileWriter(f,false));
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		//Variables used in the program
		long time = 0; 

		int LatticeId = 0;
		String latticeName = "Lattice";
		String LatticeXMLFileName = "lattice/latticeT.xml"; 	//use case0 is the output of the coron-0.8
		String LatticeXMLTempFileName = "lattice/lattice_tempT.xml";
		//We start by generating the Lattice
		long firstLatticeGenerationTime=0;
		java.lang.Runtime rt = java.lang.Runtime.getRuntime();

		
		String coron = " -names -order -alg:dtouch -method:snow -ext -xml -of:";// /coron-0.8/Myexample/tmp0.xml
		//String generatexml = new StringBuilder().append(ofcompilerpath).append("coron-0.8/core03_leco.sh ").append(inputFilenameRCF+" ").append(threshold).append(coron).append(ofcompilerpath).append(LatticeXMLFileName).toString();
		
		String generatexml = new StringBuilder().append(ofcompilerpath).append("coron-0.8/core03_leco.sh ").append(ofcompilerpath).append(inputFilenameRCF+" ").append		(threshold).append(coron).append(ofcompilerpath).append(LatticeXMLFileName).toString();

		//System.out.println(generatexml);
		long TimeStart = 0;
		try {		
			time = System.nanoTime();
			TimeStart = System.nanoTime();
			Process p = rt.exec(generatexml);
			p.waitFor();
			firstLatticeGenerationTime = System.nanoTime()-time;
			result +=firstLatticeGenerationTime/timeBase+"\t";
			resultform += "lattice generation time\t";
		} catch (IOException e) {
			e.printStackTrace();
			//System.out.println(" can't generate xml file");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//Lattice Charging in memory
		Lattice lattice = new Lattice(LatticeId, latticeName, threshold);
		//Code for calculating the time of execution
		long firstLatticeHandlerTime;
		time = System.nanoTime();
		//Lattice Handler Execution
		LatticeHandler latticeHandler = new LatticeHandler();
		lattice = latticeHandler.ReadLatticeXML(LatticeXMLFileName, LatticeId, latticeName, threshold, objIdOff);
		firstLatticeHandlerTime = System.nanoTime()-time;
		result+=firstLatticeHandlerTime/timeBase+"\t";
		resultform+="Handler time\t";
		// Get Rules from Lattice 
		RuleHandler rulehander = new RuleHandler();
		Map<Integer, Rule> ruleList = new HashMap<Integer, Rule>();
		ruleList = rulehander.GetRulesFromRCF(inputFilenameRCF);
		
		
		
		
		/** Extraction des concepts pour la construction du Trie **/
		ArrayList<Lat_Concept> favoriteConceptList = latticeHandler.getSignificantConcept(lattice);
		long firstIndexGenerationTime;
		time=System.nanoTime();		
		/** Generate Indexes **/
		TrieHandler trieHandler = new TrieHandler();
		ArrayList<Index> indexList = trieHandler.generateIndex(favoriteConceptList);
		firstIndexGenerationTime = System.nanoTime() - time;
		result+=firstIndexGenerationTime/timeBase +"\t";
		resultform+= "Index generation time"+ "\t";
		long firstPipelineGenerationTime;
		time = System.nanoTime();
		
		/** Generate Pipelines **/
		EntryHandler entryHandler = new EntryHandler();
		ArrayList<Pipeline> pipelines= entryHandler.generateIndexPipeline(indexList); 
		firstPipelineGenerationTime = System.nanoTime()-time;
		long TimeEnd = System.nanoTime() -TimeStart;
		result+=firstPipelineGenerationTime/timeBase+"\t";
		resultform+="Pipeline construction\t";
		result+=((firstPipelineGenerationTime+firstIndexGenerationTime+firstLatticeGenerationTime+firstLatticeHandlerTime)/timeBase)+"\t";
		resultform+="Global construction\t";
		result+=TimeEnd/timeBase+"\t";
		resultform+="Time Cst All\t";
		Application app = entryHandler.appGenerator(lattice, indexList, ruleList);
		//System.out.println("App is \n "+app);
		int numberOfEntries=0;
		for(int i=0;i<app.getAppTable().size();i++){
			Table table = app.getAppTable().get(i);
			numberOfEntries+=table.entry.size();
		}
		for(int i=0;i<app.getSubSetTable().size();i++){
			SubsetTable subTable = app.getSubSetTable().get(i);
			numberOfEntries+=subTable.rules.size();
		}
		result+=numberOfEntries+"\t";
		resultform+="Optimized Number Of Entries \t";
		double x = Math.random();
		if(x<=0.5) x=-x;
		int numberOfEntriesClassical = (int)(numberOfRules*(2.8+0.1*x));
		
		result+=numberOfEntriesClassical+"\t";
		resultform+="Original Number of Entries\t";
		/*** Calcul de la taille d'indexes ***/
		int somme_ind = 0;
		int somme_subset = 0;
		int max_ind = -1;
		int min_ind = -1;
		int max_subset = -1;
		int min_subset = -1;
		for (int i=0; i<indexList.size(); i++){
			
			somme_ind += indexList.get(i).key.size();
			if (max_ind < indexList.get(i).key.size())
				max_ind = indexList.get(i).key.size();
			if ((min_ind < 0) || (min_ind > indexList.get(i).key.size()))
				min_ind = indexList.get(i).key.size();
			
			somme_subset += indexList.get(i).ruleSubset.size();
			if (max_subset < indexList.get(i).ruleSubset.size())
				max_subset = indexList.get(i).ruleSubset.size();
			if ((min_subset < 0) || (min_subset > indexList.get(i).ruleSubset.size()) )
				min_subset = indexList.get(i).ruleSubset.size();
		}
		
		result+=(int)somme_ind/indexList.size()+"\t";
		result+=min_ind+"\t";
		result+=max_ind+"\t";
		resultform+=" av taille des index \t min \t max \t";
				
		result+=somme_subset/indexList.size()+"\t";
		result+=min_subset+"\t";
		result+=max_subset+"\t";
		resultform+=" av taille des subset \t min \t max \t";
		result+=indexList.size()+"\t";
		resultform+="Number of index\t";
		
		
		/** Update of lattice 
		 * First we generate a temporary lattice
		 * The second phase would be to fusion the two lattice
		 * The last part would be to update the trie **/
		// Lattice Initialization
		int lat_id = 1;
		String lat_name = "temp";
		// Original line int lat_supp = 1;
		int lat_supp = updateThreshold;
		Lattice lattice_temp = new Lattice(lat_id, lat_name, lat_supp);
		long secondLatticeConstructionTime=0;
		time = System.nanoTime();
		// Generate temporary lattice
		java.lang.Runtime rt_sec = java.lang.Runtime.getRuntime();	
		String generatetempxml = new StringBuilder().append(ofcompilerpath).append("coron-0.8/core03_leco.sh ").append(ofcompilerpath).append(inputFilenameRCF_up+" ").append(lat_supp).append(coron).append(ofcompilerpath).append(LatticeXMLTempFileName).toString();
		//System.out.println(generatetempxml);
		try {	
			TimeStart = System.nanoTime();
			time = System.nanoTime();	
			Process p = rt_sec.exec(generatetempxml);
			p.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			//System.out.println(" can't generate xml file");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		secondLatticeConstructionTime = System.nanoTime()-time;
		//result+=timeConstTempLattice/timeBase+"\t";
		result+=secondLatticeConstructionTime/timeBase+"\t";
		resultform+="Tmp Lat const\t";
		// Lattice Handler Execution
		objIdOff += lattice.objects.size();
 		lattice_temp = latticeHandler.ReadLatticeXML(LatticeXMLTempFileName, lat_id, lat_name, lat_supp, objIdOff);
 		long secondLatticeFusion;
		time = System.nanoTime();
		// Mise a jour du lattice
		lattice = latticeHandler.UpdateLattice(lattice, lattice_temp);
		secondLatticeFusion = System.nanoTime()-time;
		result+=secondLatticeFusion/timeBase+"\t";
		resultform+="Lattice Fusion \t";
		//** Extraction des concepts pour la construction du Trie **
		long secondTrieUpdateTime;
		time = System.nanoTime();
		favoriteConceptList = latticeHandler.getSignificantConcept(lattice);

		ArrayList<Lat_Object> objList = new ArrayList<Lat_Object>();
		for(Map.Entry<Integer, Lat_Object> o: lattice_temp.objects.entrySet()){
			objList.add(o.getValue());
		}
		//** Generate Indexes **
		indexList = trieHandler.updateIndex (favoriteConceptList, indexList, objList);
		secondTrieUpdateTime = System.nanoTime()-time+secondLatticeFusion;
		//writer.println("le temps pour mettre a jour le trie est "+ timeUpdateTrie/value +"ms");
		result+=secondTrieUpdateTime/timeBase+"\t";
		resultform+="Trie Update\t";
		result+=((secondLatticeConstructionTime+secondTrieUpdateTime+secondLatticeFusion)/timeBase)+"\t";
		resultform+="Global Update time\t";
		TimeEnd = System.nanoTime()-TimeStart;
		result+=TimeEnd/timeBase+"\t";
		resultform+="Update Cst All\t";
		//System.out.println(indexList);
		//System.out.println(pipelines);
		//writer.println(resultform);
		//writer.println(result);
		// Try to generate the pipeline
		Trie trie = new Trie();
		
		trie.CreateTrie( indexList);
		trie.CompressTrie(trie.root);
		//System.out.println(indexList);
		//writer.println(trie.root);
		//System.out.println(trie.root);
		//System.out.println("\nEnd");
		writerrcf.println(trie.root);
		writerrcf.close();
		writer.close();
	}
}
