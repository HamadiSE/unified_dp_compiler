#!/bin/bash
declare -i steprule
steprule=100
resultfile=entries_compression_result
for rule in `eval echo {15600..23000..$(($steprule))}`
do
	declare -i support
	if [ "$rule" -le 1000 ]
		then
		support=$(($rule/10))
	elif [ "$rule" -gt 1000 ]
		then
		support=$(($rule/27))
	fi
	declare -i update
	update=10
	Excution=-1
	test=$support
	while [ "$Excution" -ne 0 ]
		do
		echo "rule : $rule ; threshold : $test ; update : $update"
		java -jar ofcomfpiler.jar ./result/$resultfile ./input/benchmark_$rule.rcf ./input/benchmark_up_$update.rcf $test 1 $rule $update
		#echo  "java -jar compiler.jar ./result/$resultfile ./input/benchmark_$rule.rcf ./input/benchmark_up_$update.rcf $test $rule	$update"
		Excution=$?		
		test=$(($test+1))
		if [ $test -ge $(($support+10)) ]
			then
			break
		fi
		echo "Excution result is $Excution"
	done
done
rm -rf *~
